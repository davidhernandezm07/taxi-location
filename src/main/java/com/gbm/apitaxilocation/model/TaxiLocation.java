/**
 * 
 */
package com.gbm.apitaxilocation.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import com.gbm.apitaxilocation.dto.RequestTaxiLocationDto;

/**
 * Clase que representa el modelo de una hubicación de taxi.
 * 
 * @author David Hernández Muñoz
 * @version  1.0
 * @since 13/ABRIL/2019
 */
@Entity
@EntityListeners(TaxiLocationListener.class)
@Table(name = "t_taxi_location")
@Audited/******------------------ @@@@@@@ CREACION DE TABLA HISTORICA CON HIBERNATE @@@@@@@@-------------------------********/
@AuditTable("h_taxi_location")
public class TaxiLocation implements Serializable{
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1095311594261501272L;

	@EmbeddedId
	private TaxiLocationKey idLocation;
	
    @Column(name = "latitude", nullable = false)
	private Double latitude;
    
    @Column(name = "longitude", nullable = false)
	private Double longitude;
    

    
    public TaxiLocation() {}
    public TaxiLocation(RequestTaxiLocationDto requestTaxiLocationDto)
    {
    	this.idLocation=new TaxiLocationKey(requestTaxiLocationDto.getClient_id(),requestTaxiLocationDto.getVehicle_id());
    	this.idLocation.setClientId(requestTaxiLocationDto.getClient_id());
    	this.idLocation.setVehicleId(requestTaxiLocationDto.getVehicle_id());
    	
    	this.longitude=requestTaxiLocationDto.getLongitude();
    	this.latitude=requestTaxiLocationDto.getLatitude();
    }
    

	/**
	 * @return the idLocation
	 */
	public TaxiLocationKey getIdLocation() {
		return idLocation;
	}

	/**
	 * @param idLocation the idLocation to set
	 */
	public void setIdLocation(TaxiLocationKey idLocation) {
		this.idLocation = idLocation;
	}

	/**
	 * @return the latitude
	 */
	public Double getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public Double getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	
	/**
	 * @return the companiesForNotification
	 */

	@Override
	public String toString() {
		return "TaxiLocation [idLocation=" + idLocation + ", latitude=" + latitude + ", longitude=" + longitude + "]";
	}
	
	public String toJsonString() {
		return "{\"idLocation\":{\"clientId\":" +idLocation.getClientId()+",\"vehicleId\":"+idLocation.getVehicleId()+"},\"latitude\":"+latitude +",\"longitude\":" +longitude +"}";
	}




    

}
