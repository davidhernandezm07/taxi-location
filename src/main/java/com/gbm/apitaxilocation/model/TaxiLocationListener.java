package com.gbm.apitaxilocation.model;

import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gbm.apitaxilocation.service.RabbitMQSender;
/**
 * Listener de la clase TaxiLocation para realizar acciones post/pre persist and update. 
 * 
 * @author David Hernández Muñoz
 * @version  1.0
 * @since 13/ABRIL/2019
 */
@Component
public class TaxiLocationListener {

    static final Logger logger = LoggerFactory.getLogger(TaxiLocationListener.class);
    
    @Autowired
	 RabbitMQSender rabbitMQSender;
   
	
	@PostPersist
	public void postPersist(TaxiLocation taxiLocation) {
		System.out.println("Listening User Post Persist : " + taxiLocation);
		
		try {
			rabbitMQSender.RabbitMQSender(taxiLocation);
			//TimeUnit.SECONDS.sleep(5);
		} catch (Exception e) {
			 logger.error("Error al enviar mensaje a RabbitMQ");
			 logger.info("Cambiando a envio envio sincrono."); 
		}

	}
	
	@PostUpdate
	public void postUpdate(TaxiLocation taxiLocation) {
		System.out.println("Listening User Post Update : " + taxiLocation);
		try {
			rabbitMQSender.RabbitMQSender(taxiLocation);
		} catch (Exception e) {
			 logger.error("Error al enviar mensaje a RabbitMQ");
			 logger.info("Cambiando a envio envio sincrono.");
		}
	}
	
}
