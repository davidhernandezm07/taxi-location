package com.gbm.apitaxilocation.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * Clase que representa el modelo de la webhook.
 * 
 * @author David Hernández Muñoz
 * @version  1.0
 * @since 13/ABRIL/2019
 */
@Entity
@Table(name = "c_webhook")
public class WebHook {
 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="company_url")
	private String companyUrl;
	
	@Column(name="company_name")
	private String companyName;
	
	@Column(name="type")
	private int type;
	
	@Column(name="queue_name")
	private String queue;
	
 
	public WebHook() {
		super();
		// TODO Auto-generated constructor stub
	}
 


	/**
	 * @param id
	 * @param companyUrl
	 * @param companyName
	 * @param type
	 * @param queue
	 */
	public WebHook(Long id, String companyUrl, String companyName, int type, String queue) {
		super();
		this.id = id;
		this.companyUrl = companyUrl;
		this.companyName = companyName;
		this.type = type;
		this.queue = queue;
	}



	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the companyUrl
	 */
	public String getCompanyUrl() {
		return companyUrl;
	}

	/**
	 * @param companyUrl the companyUrl to set
	 */
	public void setCompanyUrl(String companyUrl) {
		this.companyUrl = companyUrl;
	}

	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * @param companyName the companyName to set
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}



	/**
	 * @return the queueName
	 */
	public String getQueue() {
		return queue;
	}



	/**
	 * @param queue the queue to set
	 */
	public void setQueue(String queue) {
		this.queue = queue;
	}



	@Override
	public String toString() {
		return "WebHook [id=" + id + ", companyUrl=" + companyUrl + ", companyName=" + companyName + ", type=" + type
				+ ", queueName=" + queue + "]";
	}

	
	

	
	
}
