package com.gbm.apitaxilocation.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Clase que representa la llave del modelo de datos de TaxiLocation
 * 
 * @author David Hernández Muñoz
 * @version  1.0
 * @since 13/ABRIL/2019
 */
@Embeddable
public class TaxiLocationKey implements Serializable {
	

	private static final long serialVersionUID = 3838566673416243034L;

	@Column(name = "client_id", nullable = false)
	 private long clientId;
	  
	 @Column(name = "vehicle_id", nullable = false)
	 private long vehicleId;

	public TaxiLocationKey() {}
	public TaxiLocationKey(Long client_id, Long vehicle_id) {
		this.clientId=client_id;
		this.vehicleId=vehicle_id;
	}


	/**
	 * @return the clientId
	 */
	public long getClientId() {
		return clientId;
	}
	/**
	 * @param clientId the clientId to set
	 */
	public void setClientId(long clientId) {
		this.clientId = clientId;
	}
	/**
	 * @return the vehicleId
	 */
	public long getVehicleId() {
		return vehicleId;
	}
	/**
	 * @param vehicleId the vehicleId to set
	 */
	public void setVehicleId(long vehicleId) {
		this.vehicleId = vehicleId;
	}
	@Override
	public String toString() {
		return "TaxiLocationKey [clientId=" + clientId + ", vehicleId=" + vehicleId + "]";
	}
	 
	 
	 
	 
	 
}
