package com.gbm.apitaxilocation.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gbm.apitaxilocation.dto.RequestTaxiLocationDto;
import com.gbm.apitaxilocation.model.TaxiLocation;
import com.gbm.apitaxilocation.model.TaxiLocationKey;
import com.gbm.apitaxilocation.service.TaxiService;
import com.gbm.apitaxilocation.service.WebHookService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Clase a cargo de manejar la lógica del los Servicios Web relacionados con el Taxi.
 * 
 * @author David Hernández Muñoz
 * @version  1.0
 * @since 13/ABRIL/2019
 */
@Api(value="Taxi Location API-Rest", description="Operations for get and add taxi's location")
@ApiResponses(value = {

	    @ApiResponse(code = 200, message = "Success"),
	    @ApiResponse(code = 201, message = "Created"),
	    @ApiResponse(code = 400, message = "Bad request, please verify the required parameters."),
	    @ApiResponse(code = 500, message = "There was an internal problem, please try it later."),
	    @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	    @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	    @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")

	})
@Controller

public class TaxiController {

    static final Logger logger = LoggerFactory.getLogger(TaxiController.class);

	
	//Service for TaxiLocation Logic
	@Autowired
	public TaxiService taxiService;
	
	//Service for TaxiLocation Logic
	@Autowired
	public WebHookService webHookService;
	

	/**
	 * Default Get WebService
	 * 
	 * */
	@RequestMapping(value = "/", method = RequestMethod.GET, produces = {MediaType.TEXT_PLAIN_VALUE})
	@ResponseBody
	public String welcome() {
		return "GBM Taxi Location API-Rest.";
	}	
	
	
	/**
	 * Web Service for inserting or updating a vehicle location.
	 * 
	 * */
	@ApiOperation(value = "Add a taxi location", response = TaxiLocation.class)
	@RequestMapping(value = "/api/taxiLocation", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public TaxiLocation createOrUpdateTaxiLocation(@Validated @RequestBody   RequestTaxiLocationDto requestDto ) {		
		 //logger.info("request: "+requestDto.toString());
		TaxiLocation taxiLocation=new TaxiLocation(requestDto);
		return taxiService.createOrUpdateTaxiLocation(taxiLocation);
		

	 }
	
	
	/**
	 * Alternative Web Service for Getting the last location for a specific vehicle.
	 * Refer to Queueing listener
	 * */
	@ApiOperation(value = "Get the last location for a specific vehicle", response = TaxiLocation.class)
	@RequestMapping(value = "/api/taxiLocationById", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public TaxiLocation getTaxiLocation(@RequestParam(value = "client_id", required = true) Long client_id,@RequestParam(value = "vehicle_id", required = true) Long vehicle_id) {
		TaxiLocationKey requestKey=	new TaxiLocationKey(client_id,vehicle_id);
		TaxiLocation taxiLocations = taxiService.getTaxiLocation(requestKey);
		return taxiLocations;
	 }
	
	
	/**
	 * Simulated Web Service for company 1
	 * 
	 * */
	@RequestMapping(value = "/company1/taxiLocationNotification", method = RequestMethod.POST, produces = {MediaType.TEXT_PLAIN_VALUE}, consumes= {MediaType.TEXT_PLAIN_VALUE})
	@ResponseBody
	public String simulatedWebServiceCompany1(@RequestBody String jsonAsString) {
		return "Company1 ok: "+jsonAsString;
	}	
		
	
	/**
	 * Simulated Web Service for company 2
	 * 
	 * */
	@RequestMapping(value = "/company2/taxNotification", method = RequestMethod.POST, produces = {MediaType.TEXT_PLAIN_VALUE},consumes= {MediaType.TEXT_PLAIN_VALUE})
	@ResponseBody
	public String simulatedWebServiceCompany2(@RequestBody String jsonAsString) {
		return "Company2 ok: "+jsonAsString;
	}	
		
	

		
}
