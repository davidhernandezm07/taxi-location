/**
 * 
 */
package com.gbm.apitaxilocation.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Clase Utility con metodos estáticos para uso genérico
 * 
 * @author David Hernández Muñoz
 * @version 1.0
 * @since 6/ABRIL/2019
 */
public final class Utility {
	
    static final Logger logger = LoggerFactory.getLogger(Utility.class);

	// Private constructor to prevent instantiation
    private Utility() {
        throw new UnsupportedOperationException();
    }
	
    public static String sendPostRequest(String requestUrl, String payload) {
        try {
            URL url = new URL(requestUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Accept", "text/plain");
            connection.setRequestProperty("Content-Type", "text/plain; charset=UTF-8");
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            writer.write(payload);
            writer.close();
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuffer jsonString = new StringBuffer();
            String line;
            while ((line = br.readLine()) != null) {
                    jsonString.append(line);
            }
            br.close();
            connection.disconnect();
            return jsonString.toString();
        } catch (Exception e) {
                
        	logger.info("Error to call notification third parthy webservices");
        	logger.error(e.toString());

        	return "Error to call notification third parthy webservices ";
        }
		

    }
	
}
