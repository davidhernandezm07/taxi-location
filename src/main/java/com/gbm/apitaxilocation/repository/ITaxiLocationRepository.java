/**
 * 
 */
package com.gbm.apitaxilocation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gbm.apitaxilocation.model.TaxiLocation;
import com.gbm.apitaxilocation.model.TaxiLocationKey;

/**
 * Interface con la definicion de repositorio de datos de la clase TaxiLocation
 * 
 * @author David Hernández Muñoz
 * @version  1.0
 * @since 13/ABRIL/2019
 */
@Repository
public interface ITaxiLocationRepository extends JpaRepository<TaxiLocation,TaxiLocationKey>{

	
}
