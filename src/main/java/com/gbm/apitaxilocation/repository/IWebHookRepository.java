/**
 * 
 */
package com.gbm.apitaxilocation.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gbm.apitaxilocation.model.WebHook;

/**
 * Interface con la definicion de repositorio de datos de la clase WebHoook
 * 
 * @author David Hernández Muñoz
 * @version  1.0
 * @since 13/ABRIL/2019
 */
@Repository
public interface IWebHookRepository extends JpaRepository<WebHook,Long>{
	
	ArrayList<WebHook> findByQueue(@Param("queue") String queue);
}
