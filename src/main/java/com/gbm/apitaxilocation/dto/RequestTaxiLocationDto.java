/**
 * 
 */
package com.gbm.apitaxilocation.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;

/**
 * DTO para la transferencia de datos para la peticion de agregar/actualizar una ubication.
 * 
 * @author David Hernández Muñoz
 * @version  1.0
 * @since 13/ABRIL/2019
 */
@ApiModel(value="TaxiLocationRequest")
public class RequestTaxiLocationDto {
	
	@NotNull(message = "Client Id is required")
    @Valid	
	private Long client_id;
	
    @Valid
 	@NotNull(message = "Vehicle Id is required")
	private Long vehicle_id;
	
    @Valid
 	@NotNull(message = "Latitude is required")
	private Double latitude;
	
    @Valid
 	@NotNull(message = "Longitude is required")
	private Double longitude;
	
	public RequestTaxiLocationDto() {
	
	} 
	

	/**
	 * @param client_id
	 * @param vehicle_id
	 * @param latitude
	 * @param longitude
	 */
	public RequestTaxiLocationDto(@NotNull Long client_id, @NotNull(message = "Vehicle Id is required") Long vehicle_id,
			@NotNull(message = "Latitude is required") Double latitude,
			@NotNull(message = "Longitude is required") Double longitude) {
		super();
		this.client_id = client_id;
		this.vehicle_id = vehicle_id;
		this.latitude = latitude;
		this.longitude = longitude;
	}


	/**
	 * @return the client_id
	 */
	public Long getClient_id() {
		return client_id;
	}

	/**
	 * @param client_id the client_id to set
	 */
	public void setClient_id(Long client_id) {
		this.client_id = client_id;
	}

	/**
	 * @return the vehicle_id
	 */
	public Long getVehicle_id() {
		return vehicle_id;
	}

	/**
	 * @param vehicle_id the vehicle_id to set
	 */
	public void setVehicle_id(Long vehicle_id) {
		this.vehicle_id = vehicle_id;
	}

	/**
	 * @return the latitude
	 */
	public Double getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public Double getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return "RequestTaxiLocationDto [client_id=" + client_id + ", vehicle_id=" + vehicle_id + ", latitude="
				+ latitude + ", longitude=" + longitude + "]";
	}

	
}
