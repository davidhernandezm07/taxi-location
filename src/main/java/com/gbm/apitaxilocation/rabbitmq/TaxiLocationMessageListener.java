
package com.gbm.apitaxilocation.rabbitmq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gbm.apitaxilocation.model.WebHook;
import com.gbm.apitaxilocation.service.WebHookService;
import com.gbm.apitaxilocation.util.Utility;
 
/**
 * Clase que continen la logica para el envio de notificaciónes
 * 
 * @author David Hernández Muñoz
 * @version  1.0
 * @since 13/ABRIL/2019
 */
@Component
public class TaxiLocationMessageListener  {
 
    static final Logger logger = LoggerFactory.getLogger(TaxiLocationMessageListener.class);
 
    @Autowired
    WebHookService WebhookService; 
    
    
    @RabbitListener(queues = RabbitMQConfig.QUEUE_TAXI_LOCATION)
    public void processLocation(Message message) {
        try
        {
        	String payload=new String(message.getBody());
	        for(WebHook s:WebhookService.findByQueue(RabbitMQConfig.QUEUE_TAXI_LOCATION))
	        {
	        	//logger just for reviewing the project functionality
	        	logger.info("Notification Service calling to: "+s.getCompanyUrl());
	        	logger.info("web service response: "+Utility.sendPostRequest(s.getCompanyUrl(),payload));
	        }
        }
        catch (Exception e) {
        	logger.info("There was an error sending notifications");
        	//logger.error(e.toString());
        	 
		} 
    
    }
    
}