package com.gbm.apitaxilocation.dao;

import java.util.List;

import com.gbm.apitaxilocation.model.TaxiLocation;

/**
 * Interface en la que se definen los metodos del DAO de taxi.
 * 
 * @author David Hernández Muñoz
 * @version  1.0
 * @since 13/ABRIL/2019
 */
public interface ITaxiDao  {
	public List<TaxiLocation> getAllTaxiLocation();


}
