package com.gbm.apitaxilocation.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.gbm.apitaxilocation.model.WebHook;
import com.gbm.apitaxilocation.repository.IWebHookRepository;
/**
 * Clase con la implementacion de los metodos de la WebHook que se comunican directamente con la fuente de datos.
 * 
 * @author David Hernández Muñoz
 * @version  1.0
 * @since 13/ABRIL/2019
 */
@Repository
public class WebHookDao  implements IWebHookDao  {
	
	 @Autowired
	 public IWebHookRepository WebHookRepository;
	
	@Override
	public List<WebHook> findByQueue(String queue) {
		return WebHookRepository.findByQueue(queue);
		
	}


}
