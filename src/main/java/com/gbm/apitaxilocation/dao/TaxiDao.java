/**
 * 
 */
package com.gbm.apitaxilocation.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.gbm.apitaxilocation.model.TaxiLocation;
import com.gbm.apitaxilocation.model.TaxiLocationKey;
import com.gbm.apitaxilocation.model.WebHook;
import com.gbm.apitaxilocation.repository.ITaxiLocationRepository;
import com.gbm.apitaxilocation.repository.IWebHookRepository;

/**
 * Clase con la implementacion de los metodos de taxi que se comunican directamente con la fuente de datos.
 * 
 * @author David Hernández Muñoz
 * @version  1.0
 * @since 13/ABRIL/2019
 */
@Repository
public class TaxiDao implements ITaxiDao  {
	
	 @Autowired
	 public ITaxiLocationRepository taxiLocationRepository;
	 
	 @Autowired
	 public IWebHookRepository WebHookRepository;
	 
	public List<TaxiLocation> getAllTaxiLocation(){
		return taxiLocationRepository.findAll();
	}

	public TaxiLocation createOrUpdateTaxiLocation(TaxiLocation taxiLocation) {
		return taxiLocationRepository.save(taxiLocation);
	
		
	}
	
	public TaxiLocation getTaxiLocationById(TaxiLocationKey taxiLocationKey ) {
		 Optional<TaxiLocation> result = taxiLocationRepository.findById(taxiLocationKey);
		 if(result.isPresent())
			 return result.get();
		 else
			 return  new TaxiLocation();
			
	}
	

	
	public List<WebHook> getCompanyInfoByOperationTypes() {
		return WebHookRepository.findAll();
		
	}
}
