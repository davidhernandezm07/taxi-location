package com.gbm.apitaxilocation.dao;

import java.util.List;

import com.gbm.apitaxilocation.model.WebHook;

/**
 * Interface en la que se definen los metodos del DAO de la webhook.
 * 
 * @author David Hernández Muñoz
 * @version  1.0
 * @since 13/ABRIL/2019
 */
public interface IWebHookDao  {
	public List<WebHook> findByQueue(String queue);

}
