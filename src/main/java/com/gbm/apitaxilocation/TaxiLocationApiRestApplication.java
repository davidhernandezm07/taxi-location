package com.gbm.apitaxilocation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * Clase principal de spring boot
 * 
 * @author David Hernández Muñoz
 * @version  1.0
 * @since 13/ABRIL/2019
 */
@SpringBootApplication
public class TaxiLocationApiRestApplication {
	public static void main(String[] args) {
		try {
			SpringApplication.run(TaxiLocationApiRestApplication.class, args);

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
