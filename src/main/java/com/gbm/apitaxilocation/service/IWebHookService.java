package com.gbm.apitaxilocation.service;

import java.util.List;

import com.gbm.apitaxilocation.model.WebHook;
/**
 * Interface con la definicion de los metodos de la clase WebHookService
 * 
 * @author David Hernández Muñoz
 * @version 1.0
 * @since 6/ABRIL/2019
 * 
 */
public interface IWebHookService {

	List<WebHook> findByQueue(String queue);

}
