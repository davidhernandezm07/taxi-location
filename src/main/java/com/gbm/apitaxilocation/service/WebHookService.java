/**
 * 
 */
package com.gbm.apitaxilocation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gbm.apitaxilocation.dao.WebHookDao;
import com.gbm.apitaxilocation.model.WebHook;
/**
 * Clase service con los metodos puntuales para la interacción con la capa de datos.
 * 
 * @author David Hernández Muñoz
 * @version 1.0
 * @since 6/ABRIL/2019
 */
@Service
public class WebHookService implements IWebHookService {

	@Autowired
	private WebHookDao webHookDao;
	
	@Override
	public List<WebHook> findByQueue(String queue) {
		
		return webHookDao.findByQueue(queue);
	}
	


}
