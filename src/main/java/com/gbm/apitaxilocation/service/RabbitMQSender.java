package com.gbm.apitaxilocation.service;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gbm.apitaxilocation.model.TaxiLocation;
import com.gbm.apitaxilocation.rabbitmq.RabbitMQConfig;
/**
 * Clase service con la definición de RabbitMQ para el encolamiento de mensajes
 * 
 * @author David Hernández Muñoz
 * @version 1.0
 * @since 6/ABRIL/2019
 * 
 */
@Service
public class RabbitMQSender {
	
    private final RabbitTemplate rabbitTemplate;
    
    @Autowired
    public RabbitMQSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

	public void RabbitMQSender(TaxiLocation taxiLocation) {
        this.rabbitTemplate.convertAndSend(RabbitMQConfig.QUEUE_TAXI_LOCATION, taxiLocation);
    }
	

}