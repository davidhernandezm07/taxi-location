package com.gbm.apitaxilocation.service;

import java.util.List;

import com.gbm.apitaxilocation.model.TaxiLocation;
import com.gbm.apitaxilocation.model.TaxiLocationKey;
/**
 * Interface con la definicion de los metodos de la clase TaxiService
 * 
 * @author David Hernández Muñoz
 * @version  1.0
 * @since 13/ABRIL/2019
 */
public interface ITaxiService {
	public List<TaxiLocation>getAllTaxiLocation();
	public TaxiLocation createOrUpdateTaxiLocation(TaxiLocation taxiLocation);
	public TaxiLocation getTaxiLocation(TaxiLocationKey taxiLocationKey);

}
