/**
 * 
 */
package com.gbm.apitaxilocation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gbm.apitaxilocation.dao.TaxiDao;
import com.gbm.apitaxilocation.model.TaxiLocation;
import com.gbm.apitaxilocation.model.TaxiLocationKey;

/**
 * Clase service con los metodos puntuales para la interacción con la capa de datos.
 * 
 * @author David Hernández Muñoz
 * @version 1.0
 * @since 6/ABRIL/2019
 */
@Service
public class TaxiService implements ITaxiService {

	@Autowired
	private TaxiDao taxiDao;
	
	@Override
	public List<TaxiLocation> getAllTaxiLocation() {
		
		return taxiDao.getAllTaxiLocation();
	}
	
	@Override
	public TaxiLocation createOrUpdateTaxiLocation(TaxiLocation taxiLocation) {
		return taxiDao.createOrUpdateTaxiLocation(taxiLocation);
		
	}
	@Override
	public TaxiLocation getTaxiLocation(TaxiLocationKey taxiLocationKey) {
		return taxiDao.getTaxiLocationById(taxiLocationKey);
	}

}
