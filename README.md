This a springboot application, it uses a RabbitMQ broker for sending messages.

RabbitMQ is used for sending messagess to a queue, there is a listener in charge of taking these messages
and send them as notification to every url comany registered.

The application perform create/update and get operations without RabbitMQ.
It's possible to skeep this step 1 and go to step 2,it just notifications wont be sent.

1. Intall and run RabbitMQ
	Windows
		https://www.rabbitmq.com/install-windows.html
	Mac	
		https://www.rabbitmq.com/install-homebrew.html 
	Ubuntu/Debian
		https://www.rabbitmq.com/install-debian.html

2. Start the application
	java -jar api-taxi-location-0.0.1-SNAPSHOT.jar
	
API-Rest Web Service Documentation is available on:
	http://localhost:8080/swagger-ui.html#/

Data is stored in an h2 embebbed database.
User Graphic intreface is availble on:
	http://localhost:8080/h2
	
	JDBC UR:jdbc:h2:mem:testdb 
	User Name:sa
	Password:
	
	
	